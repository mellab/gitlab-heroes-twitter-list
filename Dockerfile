FROM golang:1.14-buster as build

RUN apt update && apt install --no-install-recommends -y git make
WORKDIR /go/src/gitlab.com/m4r10k/gitlab-heroes-twitter-list
COPY go.mod go.mod 
COPY go.sum go.sum
RUN go mod download
COPY . .
ENV CGO_ENABLED=0
ENV GO111MODULE=on
RUN make


FROM debian:buster-slim
RUN apt update && apt install --no-install-recommends -y ca-certificates 
COPY --from=build /go/bin/gitlab-heroes-twitter-list /bin/gitlab-heroes-twitter-list
CMD ["/bin/gitlab-heroes-twitter-list"]
